const routeIcons = {
  home: 'home',
  socialNetworks: 'global',
  staffs: 'team',
  introductContent: 'appstore',
  sponsors: 'bank',
  intent: 'star',
  policies: 'tool',
  terms: 'api',
  recruitment: 'user-add',
};

const menu = {
  home: 'Home',
  socialNetworks: 'Social Networks',
  staffs: 'Staffs',
  introductContent: 'Introduct Content',
  sponsors: 'Sponsors',
  intent: 'Intent',
  policies: 'Policies',
  terms: 'Terms',
  recruitment: 'Recruiment',
};

export { routeIcons, menu };
