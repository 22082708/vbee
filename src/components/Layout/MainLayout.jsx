import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';
import styles from './MainLayout.scss';
import logo from '../../../public/images/vbee-logo.png';
import logoIco from '../../../public/images/favicon.ico';
import { router } from '../../containers/App/config';
import { routeIcons, menu } from './config';

const {
  Header, Content, Footer, Sider,
} = Layout;
const { SubMenu } = Menu;

class MainLayout extends React.Component {
  constructor() {
    super();
    this.state = {
      collapsed: false,
    };
  }
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <Link to="/" style={{ textDecoration: 'none' }}>
            <div className={styles.logo}>
              <div className={styles.brand}>
                {this.state.collapsed ? (
                  <img src={logoIco} width={21} height={21} alt="logo_ico" />
                ) : (
                  <img src={logo} width={70} height={21} alt="logo" />
                )}
              </div>
            </div>
          </Link>
          <Menu theme="dark" mode="inline">
            <SubMenu
              key="menu1"
              title={
                <span>
                  <Icon type="android-o" />
                  <span>Menu 1</span>
                </span>
              }
            >
              <Menu.Item key="submenu1.1">
                <Link to="/sub-menu-1">
                  <Icon type="facebook" />
                  <span>Sub Menu 1</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="submenu1.2">
                <Link to="/sub-menu-2">
                  <Icon type="twitter" />
                  <span>Sub Menu 2</span>
                </Link>
              </Menu.Item>
            </SubMenu>
            <Menu.Item key="menu2">
              <Link to="/menu-2">
                <Icon type="apple-o" />
                <span>Menu 2</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={router.socialNetworks}>
              <Link to={router.socialNetworks}>
                <Icon type={routeIcons.socialNetworks} />
                <span>{menu.socialNetworks}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={router.intent}>
              <Link to={router.intent}>
                <Icon type={routeIcons.intent} />
                <span>{menu.intent}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={router.introductContent}>
              <Link to={router.introductContent}>
                <Icon type={routeIcons.introductContent} />
                <span>{menu.introductContent}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={router.policies}>
              <Link to={router.policies}>
                <Icon type={routeIcons.policies} />
                <span>{menu.policies}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={router.recruitment}>
              <Link to={router.recruitment}>
                <Icon type={routeIcons.recruitment} />
                <span>{menu.recruitment}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={router.sponsors}>
              <Link to={router.sponsors}>
                <Icon type={routeIcons.sponsors} />
                <span>{menu.sponsors}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={router.staffs}>
              <Link to={router.staffs}>
                <Icon type={routeIcons.staffs} />
                <span>{menu.staffs}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={router.terms}>
              <Link to={router.terms}>
                <Icon type={routeIcons.terms} />
                <span>{menu.terms}</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header
            style={{
              background: '#222632',
              padding: 0,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
            id="components-layout-demo-custom-trigger"
          >
            <Icon
              className="trigger"
              style={{ color: '#fff', width: '5%' }}
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
            <span
              style={{
                float: 'right',
                paddingRight: '10px',
                textTransform: 'uppercase',
                width: '90%',
                textAlign: 'right',
                // fontWeight: 'bold',
                // fontSize: '25px',
                font: 'normal 30px/normal "Warnes", Helvetica, sans-serif',
                color: 'rgba(255,255,255,1)',
                textShadow: '0 0 30px #00ffff',
              }}
            >
              Vbee Content Management System
            </span>
          </Header>
          <Content style={{ margin: '0 16px' }}>{this.props.children}</Content>
          <Footer
            style={{
              textAlign: 'center',
              background: '#222632',
              color: '#fff',
              fontWeight: 'bold',
            }}
          >
            Vbee ©2018
          </Footer>
        </Layout>
      </Layout>
    );
  }
}

MainLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default MainLayout;
