import React from "react";
import PropTypes from "prop-types";
import "../../../public/css/ionicons.css";

export default class Ionicon extends React.Component {
  render() {
    const {
      icon, className,
      onClick,
    } = this.props;
    const style = {
      ...{
        fontSize: '25px'
      },
      ...this.props.style,
    };
    return (
      <i
        className={`icon ${icon} ${className}`}
        style={style}
        onClick={onClick}
        onKeyPress={onClick}
        role="button"
        tabIndex={-1}
      />
    );
  }
}

Ionicon.propTypes = {
  icon: PropTypes.string.isRequired,
  style: PropTypes.shape,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

Ionicon.defaultProps = {
  style: {},
  className: "",
  onClick: () => {},
};
