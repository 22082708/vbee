import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Input, Button, message } from 'antd';
import FormItem from "antd/es/form/FormItem";
import Order from "./Order";
import Avatar from "./Avatar";

class Edit extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        const {
            visible, onCancel, form,
        } = this.props;
        const { getFieldDecorator } = form;
        var {object} = this.props;
        return (
            <Modal
                visible={visible}
                title="Edit this sponsor"
                onCancel={onCancel}
                okText="Lưu"
                cancelText="Hủy"
            >
                <Form layout="vertical">
                    <FormItem label="Name of Sponsor">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa nhập thông tin!',
                                },
                            ],
                        })(<div><Input defaultValue={object.name.first + " " + object.name.last}/></div>)}
                    </FormItem>
                    <FormItem label="Url">
                        {getFieldDecorator('url', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa nhập Url!',
                                },
                            ],
                        })(<div><Input defaultValue={object.email}/></div>)}
                    </FormItem>
                    <FormItem label="Order">
                        {getFieldDecorator('Order', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa chọn mức ưu tiên!',
                                },
                            ],
                        })(<Order />)}
                    </FormItem>
                    <FormItem label="Avatar">
                        {getFieldDecorator('Avatar', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa upload Avatar!',
                                },
                            ],
                        })(<Avatar />)}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}

Edit.propTypes = {
    visible: PropTypes.bool.isRequired,
    onCancel: PropTypes.func.isRequired,
    form: PropTypes.node.isRequired,
};

export default Form.create()(Edit);
