import React from 'react';
import PropTypes from 'prop-types';
import { Table, Select } from 'antd';
import { connect } from 'react-redux';
import uuid from 'uuid';
import { getUsersRequest } from '../../actions/user';
import { capitalizeFirstLetter } from './helperFunctions';
import './index.css';
import ActionHeader from './ActionHeader';
import ActionButtonGroup from './ActionButtonGroup';
import Order from "./Order";

const columns = [
    {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        width: '7%',
        render: (text, record) => `${record.index + 1}`,
    },
    {
        title: 'Logo',
        dataIndex: 'logo',
        key: 'logo',
        width: '8%',
        render: (text, record) => (
            <img
                src={record.picture.thumbnail}
                width={50}
                height={50}
                alt="thumbnail"
            />
        ),
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: '20%',
        render: (text, record) =>
            `${capitalizeFirstLetter(record.name.first)} ${capitalizeFirstLetter(record.name.last)}`,
    },
    {
        title: 'Url',
        dataIndex: 'url',
        key: 'url',
        width: '30%',
        render: (text, record) => record.email,
    },
    {
        title: 'Order',
        dataIndex: 'order',
        key: 'order',
        width: '20%',
        // render: (text, record) => record.phone,
        render(){
            return (
                <Order />
            )
        }
    },
    {
        title: <ActionHeader />,
        dataIndex: 'action',
        key: 'action',
        width: '15%',
        render: (text, record) => <ActionButtonGroup object={record}/>,
    },
];

class Sponsors extends React.Component {
    constructor() {
        super();
        this.state = {
            users: [],
        };
    }
    componentDidMount() {
        this.props.getUsers(20);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            users: nextProps.users.map((user, index) => ({
                ...user,
                index,
                key: uuid.v4(),
            })),
        });
    }
    render() {
        const { loading } = this.props;
        return (
            <div className="sponsors">
                <h2 className="sponsors-title">List of sponsors</h2>
                <Table
                    dataSource={this.state.users}
                    columns={columns}
                    loading={loading}
                    bordered
                    pagination={{ pageSize: 5 }}
                    className="sponsors-table"
                />
            </div>
        );
    }
}

Sponsors.propTypes = {
    users: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.shape({
            first: PropTypes.string.isRequired,
            last: PropTypes.string.isRequired,
        }),
        picture: PropTypes.shape({
            thumbnail: PropTypes.string.isRequired,
        }),
        email: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired,
    })).isRequired,
    loading: PropTypes.bool.isRequired,
    getUsers: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    users: state.userReducer.users,
    loading: state.userReducer.loading,
});

const mapDispatchToProps = dispatch => ({
    getUsers: amount => dispatch(getUsersRequest(amount)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sponsors);
