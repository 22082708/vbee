import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Input, Button, message } from 'antd';
import FormItem from "antd/es/form/FormItem";
import Order from "./Order";
import Avatar from "./Avatar";

class SponsorsCreateForm extends React.Component {
    render() {
        const {
            visible, onCancel, form,
        } = this.props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                visible={visible}
                title="Add a new sponsor"
                onCancel={onCancel}
                okText="Lưu"
                cancelText="Hủy"
            >
                <Form layout="vertical">
                    <FormItem label="Name of Sponsor">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa nhập thông tin!',
                                },
                            ],
                        })(<Input />)}
                    </FormItem>
                    <FormItem label="Url">
                        {getFieldDecorator('url', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa nhập Url!',
                                },
                            ],
                        })(<Input />)}
                    </FormItem>
                    <FormItem label="Order">
                        {getFieldDecorator('Order', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa chọn mức ưu tiên!',
                                },
                            ],
                        })(<Order />)}
                    </FormItem>
                    <FormItem label="Avatar">
                        {getFieldDecorator('Avatar', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa upload Avatar!',
                                },
                            ],
                        })(<Avatar />)}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}

SponsorsCreateForm.propTypes = {
    visible: PropTypes.bool.isRequired,
    onCancel: PropTypes.func.isRequired,
    form: PropTypes.node.isRequired,
};

export default Form.create()(SponsorsCreateForm);
