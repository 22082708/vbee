/* eslint-disable no-unused-vars */
import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import MainLayout from '../../components/Layout/MainLayout';
import UserList from '../../containers/UserList/UserList';
import Demo from '../Demo';
import IntroductContent from '../IntroductContent';
import SocialNetworks from '../SocialNetworks';
import Sponsors from '../Sponsors';

import { router } from './config';

function App() {
  const {
    home,
    socialNetworks, sponsors, introductContent,
    staffs, policies, terms,
    intent, recruitment,
  } = router;
  return (
    <BrowserRouter>
      <Switch>
        <MainLayout>
          <Switch>
            <Route exact path={home} component={Demo} />
            <Route exact path={introductContent} component={IntroductContent} />
            <Route exact path={socialNetworks} component={SocialNetworks} />
            <Route exact path={sponsors} component={Sponsors} />
          </Switch>
        </MainLayout>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
