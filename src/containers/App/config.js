const router = {
  home: '/',
  socialNetworks: '/social-networks',
  staffs: '/staffs',
  introductContent: '/introduct-content',
  sponsors: '/sponsors',
  intent: '/intent',
  policies: '/policies',
  terms: '/terms',
  recruitment: '/recruitment',
};

export { router };
