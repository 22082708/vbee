import React from 'react';

class Order extends React.Component{
    render(){
        const order = [1,2,3,4,5,6,7,8,9,10];
        const orderOption = order.map( order => <option key={order}>{order} </option>);
        return (
            <div>
                <select defaultValue={order[0]} style={{width: 200}} >
                    {orderOption};
                </select>
            </div>
        )
    }
}

export default Order;