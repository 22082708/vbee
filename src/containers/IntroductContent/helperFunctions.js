import {message} from "antd/lib/index";
import React from "react";

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}



export { capitalizeFirstLetter };
