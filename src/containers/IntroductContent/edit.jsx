import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Input, Select, Upload, message, Button, Icon } from 'antd';
import FormItem from "antd/es/form/FormItem";
import Order from "./Order";
import Avatar from "./Avatar";

class Edit extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        const {
            visible, onCancel, form,
        } = this.props;
        const { getFieldDecorator } = form;
        var {object}=this.props;
        return (
            <Modal
                visible={visible}
                title="Edit this introduction"
                onCancel={onCancel}
                okText="Lưu"
                cancelText="Hủy"
            >
                <Form layout="vertical">
                    <Form.Item label="Tên người dùng">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    required: true,
                                    message: 'chưa nhập tên người dùng!',
                                },
                            ],
                        })(<div><Input defaultValue={object.name.first + " " + object.name.last}/></div>)}
                    </Form.Item>
                    <FormItem label="Content">
                        {getFieldDecorator('content', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa nhập nội dung!',
                                },
                            ],
                        })(<div><Input defaultValue={object.email}/></div>)}
                    </FormItem>
                    <FormItem label="Order">
                        {getFieldDecorator('Order', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa chọn mức ưu tiên!',
                                },
                            ],
                        })(<Order />)}
                    </FormItem>
                    <FormItem label="Logo">
                        {getFieldDecorator('Logo', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa upload Logo!',
                                },
                            ],
                        })(<Avatar />)}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}

Edit.propTypes = {
    visible: PropTypes.bool.isRequired,
    onCancel: PropTypes.func.isRequired,
    form: PropTypes.node.isRequired,
};

export default Form.create()(Edit);
