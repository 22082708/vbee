import React from 'react';
import uuid from 'uuid';
import { Tooltip, Button } from 'antd';
import UserCreateForm from './SocialNetworksCreateForm';

class ActionHeader extends React.Component {
  constructor() {
    super();
    this.state = { showUserCreateForm: false };
  }
  showUserCreateForm = () => {
    this.setState({ showUserCreateForm: true });
  }
  onCancelCreateUser = () => {
    this.setState({ showUserCreateForm: false });
  }
  onCreateUser = () => {
    this.form.validateFields((err, values) => {
      if (err) {
        return;
      }

      console.log(values);

      this.form.resetFields();
      this.setState({ showUserCreateForm: false });
    });
  }
  render() {
    return [
      <span key={uuid.v4()}>
        Hành động &nbsp;
        <Tooltip title="Thêm người dùng">
          <Button shape="circle" icon="plus" onClick={this.showUserCreateForm} />
        </Tooltip>
      </span>,
      <UserCreateForm
        key={uuid.v4()}
        ref={(form) => { this.form = form; }}
        visible={this.state.showUserCreateForm}
        onCancel={this.onCancelCreateUser}
        onCreate={this.onCreateUser}
      />,
    ];
  }
}

export default ActionHeader;
