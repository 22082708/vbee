import React from 'react';
import { Tooltip, Button } from 'antd';
import UserUpdateForm from './edit';
import uuid from 'uuid';

export default class ActionButtonGroup extends React.Component {
    constructor() {
        super();
        this.state = { showUserUpdateForm: false };
    }
    showUserUpdateForm = () => {
        this.setState({ showUserUpdateForm: true });
    }
    onCancelUpdateUser = () => {
        this.setState({ showUserUpdateForm: false });
    }
    onUpdateUser = () => {
        this.form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log(values);

            this.form.resetFields();
            this.setState({ showUserUpdateForm: false });
        });
    }

    render() {
        var {object}=this.props;
        return (
            <div>
                <span key={uuid.v4()}>
                <Tooltip title="Sửa">
                    <Button
                        shape="circle"
                        icon="edit"
                        style={{ color: 'orange' }}
                        onClick={() => {this.showUserUpdateForm()}}
                    />
                </Tooltip>
                <Tooltip title="Xóa">
                    <Button
                        shape="circle"
                        icon="delete"
                        style={{ color: 'red' }}
                    />
                </Tooltip>
                </span>,
                <UserUpdateForm
                    key={uuid.v4()}
                    ref={(form) => { this.form = form; }}
                    visible={this.state.showUserUpdateForm}
                    onCancel={this.onCancelUpdateUser}
                    onUpdate={this.onUpdateUser}
                    object={object}
                />,
            </div>
        );
    }
}
