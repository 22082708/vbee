import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Input, Select, Upload, message, Button, Icon } from 'antd';
import FormItem from "antd/es/form/FormItem";
import Order from "./Order";
import Avatar from "./Avatar";

class SocialNetworksCreateForm extends React.Component {
    render() {
        const {
            visible, onCancel, form,
        } = this.props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                visible={visible}
                title="Thêm mạng xã hội"
                onCancel={onCancel}
                okText="Lưu"
                cancelText="Hủy"
            >
                <Form layout="vertical">
                    <FormItem label="Tên mạng xã hội">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa nhập tên mạng xã hội!',
                                },
                            ],
                        })(<Input />)}
                    </FormItem>
                    <FormItem label="Order">
                        {getFieldDecorator('Order', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa chọn mức ưu tiên!',
                                },
                            ],
                        })(<Order />)}
                    </FormItem>
                    <FormItem label="Avatar">
                        {getFieldDecorator('Avatar', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa upload Avatar!',
                                },
                            ],
                        })(<Avatar />)}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}

SocialNetworksCreateForm.propTypes = {
    visible: PropTypes.bool.isRequired,
    onCancel: PropTypes.func.isRequired,
    form: PropTypes.node.isRequired,
};

export default Form.create()(SocialNetworksCreateForm);
