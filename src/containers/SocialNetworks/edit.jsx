import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Input, Select, Upload, message, Button, Icon } from 'antd';
import FormItem from "antd/es/form/FormItem";
import Order from "./Order";
import Avatar from "./Avatar";

class Edit extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        const {
            visible, onCancel, form,
        } = this.props;
        const { getFieldDecorator } = form;
        var {object}=this.props;
        return (
            <Modal
                visible={visible}
                title="Thêm mạng xã hội"
                onCancel={onCancel}
                okText="Lưu"
                cancelText="Hủy"
            >
                <Form layout="vertical">
                    <FormItem label="Tên mạng xã hội">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa nhập tên mạng xã hội!',
                                },
                            ],
                        })(<div><Input defaultValue={object.name.first + " " + object.name.last}/></div>)}
                    </FormItem>
                    <FormItem label="Order">
                        {getFieldDecorator('Order', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa chọn mức ưu tiên!',
                                },
                            ],
                        })(<div><Input defaultValue={object.email}/></div>)}
                    </FormItem>
                    <FormItem label="Avatar">
                        {getFieldDecorator('Avatar', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Chưa upload Avatar!',
                                },
                            ],
                        })(<Avatar />)}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}

Edit.propTypes = {
    visible: PropTypes.bool.isRequired,
    onCancel: PropTypes.func.isRequired,
    form: PropTypes.node.isRequired,
};

export default Form.create()(Edit);
