import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Input } from 'antd';

class UserCreateForm extends React.Component {
  render() {
    const {
      visible, onCancel, form,
    } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Modal
        visible={visible}
        title="Thêm người dùng mới"
        onCancel={onCancel}
        okText="Lưu"
        cancelText="Hủy"
      >
        <Form layout="vertical">
          <Form.Item label="Tên người dùng">
            {getFieldDecorator('name', {
              rules: [
                {
                  required: true,
                  message: 'Chưa nhập tên người dùng!',
                },
              ],
            })(<Input />)}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

UserCreateForm.propTypes = {
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  form: PropTypes.node.isRequired,
};

export default Form.create()(UserCreateForm);
