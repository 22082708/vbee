import React from 'react';
import { Tooltip, Button } from 'antd';

export default class ActionButtonGroup extends React.Component {
  render() {
    return (
      <div>
        <Tooltip title="Sửa">
          <Button
            shape="circle"
            icon="edit"
            style={{ color: 'orange' }}
          />
        </Tooltip>
        <Tooltip title="Xóa">
          <Button
            shape="circle"
            icon="delete"
            style={{ color: 'red' }}
          />
        </Tooltip>
      </div>
    );
  }
}
