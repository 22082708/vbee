import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';

import 'antd/dist/antd.css';
import './index.scss';

import App from './containers/App/App';

/* Redux Devtools - Redux Logger */
// import configureStore from './store/configureStore';
// import DevTools from './containers/DevTools';

// const store = configureStore();
// const AppDevTools = <div><App /><DevTools /></div>;

/* Redux Devtools Extension - Redux Logger */
import store from './store';

const AppDevTools = <App />;
/* End of Redux Devtools Extension - Redux Logger */

ReactDOM.render(
  <AppContainer>
    <Provider store={store}>
      {AppDevTools}
    </Provider>
  </AppContainer>,
  document.getElementById('root'),
);
