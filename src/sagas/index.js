import { takeEvery, all } from 'redux-saga/effects';
import { GET_USERS_REQUEST } from '../actions/user';
import getUsersWorker from './userSaga';

export default function* rootSaga() {
  yield all([
    takeEvery(GET_USERS_REQUEST, getUsersWorker),
  ]);
}
