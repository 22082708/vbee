import { call, put, select } from 'redux-saga/effects';
import { fetchRandomUsers } from '../apis/user';
import { getUsersSuccess, getUsersFailure } from '../actions/user';

export default function* getUsersWorker(action) {
  try {
    const { amount } = action.payload;
    const { users } = yield call(fetchRandomUsers, amount);
    if (users) {
      yield put(getUsersSuccess(users));
    } else throw new Error();
  } catch (error) {
    yield put(getUsersFailure());
  }
}
