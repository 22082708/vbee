const configureStoreDevelopment = require('./configureStore.dev');
const configureStoreProduction = require('./configureStore.prod');

if (process.env.NODE_ENV === 'production') {
  module.exports = configureStoreProduction;
} else {
  module.exports = configureStoreDevelopment;
}
